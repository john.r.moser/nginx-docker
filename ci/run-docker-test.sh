#!/bin/bash

# Print commands
set -x

export COMPOSE_PROJECT_NAME="${BUILD_TAG##*/}"

compose_test() {
  # Start up nginx
  docker-compose -f ci/docker-compose.ci.yml up -d nginx || return $?

  # Run test
  docker-compose -f ci/docker-compose.ci.yml up test || return $?
}


sed -e 's#WORKDIR#'"$WORKSPACE"'#g' ci/docker-compose.ci.yml.tmpl \
 > ci/docker-compose.ci.yml

compose_test
e="$?"

# Remove containers
docker-compose -f ci/docker-compose.ci.yml stop
docker-compose -f ci/docker-compose.ci.yml rm -f

exit $e
